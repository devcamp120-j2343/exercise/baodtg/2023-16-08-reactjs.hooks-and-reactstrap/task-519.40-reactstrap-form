import { Button, Col, Container, Label, Row } from "reactstrap"
import 'bootstrap/dist/css/bootstrap.min.css'
import avatar from '../assets/images/3d-illustration-person-with-sunglasses_23-2149436188.avif'

function ReactstrapForm() {
    return (
        <>
            <Container >
                <div className="mt-5 text-center">
                    <h3 className="title">HỒ SƠ NHÂN VIÊN</h3>
                </div>
                <Row>
                    <Col md={8}>
                        <Row className="d-flex align-items-center mt-3">
                            <Col md={3}>
                                <Label>Họ và tên</Label>
                            </Col>
                            <Col md={9} >
                                <input className="w-100" style={{ height: "40px" }} />
                            </Col>
                        </Row>
                        <Row className="d-flex align-items-center mt-3">
                            <Col md={3}>
                                <Label>Ngày sinh</Label>
                            </Col>
                            <Col md={9} >
                                <input className="w-100" style={{ height: "40px" }} />
                            </Col>
                        </Row>
                        <Row className="d-flex align-items-center mt-3">
                            <Col md={3}>
                                <Label>Số điện thoại</Label>
                            </Col>
                            <Col md={9} >
                                <input className="w-100" style={{ height: "40px" }} />
                            </Col>
                        </Row>
                        <Row className="d-flex align-items-center mt-3">
                            <Col md={3}>
                                <Label>Giới tính</Label>
                            </Col>
                            <Col md={9} >
                                <input className="w-100" style={{ height: "40px" }} />
                            </Col>
                        </Row>
                    </Col>
                    <Col md={4} className="mt-3">
                        <img src={avatar} alt="avatar" width={"210px"}></img>
                    </Col>
                </Row>
                <Row className="mt-3">
                    <Col md={2}>
                        <Label>Công việc</Label>
                    </Col>
                    <Col md={10}>
                        <textarea className="w-100" style={{ height: "80px" }}></textarea>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-end">
                        <Button color="success" className="me-2">Chi tiết</Button>
                        <Button color="success">Kiểm tra</Button>

                    </Col>
                </Row>

            </Container>

        </>
    )
}
export default ReactstrapForm